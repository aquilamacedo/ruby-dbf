ruby-dbf (4.2.4-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on rake.
  * Update standards version to 4.6.2, no changes needed.

  [ Daniel Leidert ]
  * New upstream version 4.2.0

  [ Aquila Macedo ]
  * New upstream version 4.2.4
  * d/patches: Remove the obsolete '0002-Fix-Ruby-3-compatibility.patch' patch.

 -- Aquila Macedo Costa <aquilamacedo@riseup.net>  Sun, 29 Oct 2023 17:45:22 -0300

ruby-dbf (3.0.5-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Daniel Leidert ]
  * d/control: Add Rules-Requires-Root field.
    (Standards-Version): Bump to 4.6.0.
    (Depends): Use ${ruby:Depends}.
  * d/copyright: Add Upstream-Contact field.
    (Source): Use secure URL.
    (Copyright): Add team.
  * d/rules: Use gem installation layout.
  * d/watch: Use gemwatch URL.
  * d/patches/0002-Fix-Ruby-3-compatibility.patch: Add patch.
    - Fix compatibility with Ruby 3 (closes: #996215).
  * d/patches/series: Enable new patch.
  * d/upstream/metadata: Add missing fields.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 15 Nov 2021 19:19:17 +0100

ruby-dbf (3.0.5-1) unstable; urgency=medium

  * Team upload
  * Imported Upstream version 3.0.5
    + does not require fastercvs in gemspecs (Closes: #830098)
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.8 (no changes needed)
  * Run wrap-and-sort on packaging files
  * Check gem dependencies in debian/rules
  * Remove hack to use the -s switch of the interpreter
  * Set default encoding to UTF-8 in tests

 -- Cédric Boutillier <boutil@debian.org>  Fri, 08 Jul 2016 16:24:49 +0200

ruby-dbf (2.0.10-1) unstable; urgency=medium

  * Imported Upstream version 2.0.10
  * Update my email address

 -- Christopher Baines <mail@cbaines.net>  Sat, 18 Jul 2015 21:49:54 +0100

ruby-dbf (2.0.7-1) unstable; urgency=medium

  * Team upload.

  [ David Suárez ]
  * New upstream version.
  * debian/changelog: set urgency to medium.

 -- David Suárez <david.sephirot@gmail.com>  Sun, 05 Oct 2014 15:24:17 +0200

ruby-dbf (2.0.6-2) unstable; urgency=low

  * Team upload.
  * Rewrite shebang of /usr/bin/dbf-rb, to be able to use -s ruby switch.
    (Closes: #731184)

 -- Cédric Boutillier <boutil@debian.org>  Tue, 03 Dec 2013 18:44:57 +0100

ruby-dbf (2.0.6-1) unstable; urgency=low

  * Team upload
  * Imported Upstream version 2.0.6
  * Build against gem2deb (>=0.5) to add Ruby2.0 support and drop Ruby1.8
  * Drop dependency on ruby-fastercsv (Ruby1.8 only)
  * Bump Standards-Version to 3.9.5 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Tue, 03 Dec 2013 08:08:16 +0100

ruby-dbf (2.0.5-1) unstable; urgency=low

  * New upstream version
  * Remove patches as they have been merged upstream

 -- Christopher Baines <cbaines8@gmail.com>  Sat, 06 Jul 2013 16:45:35 +0100

ruby-dbf (2.0.3-1) unstable; urgency=low

  * Initial release (Closes: #702161)

 -- Christopher Baines <cbaines8@gmail.com>  Sun, 24 Feb 2013 16:47:07 +0000
